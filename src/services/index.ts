import { Application } from '../declarations';
import signups from './signups/signups.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application) {
  app.configure(signups);
}
